package ru.t1.zkovalenko.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.repository.ISessionRepository;
import ru.t1.zkovalenko.tm.api.repository.IUserOwnerRepository;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.ISessionService;
import ru.t1.zkovalenko.tm.model.Session;
import ru.t1.zkovalenko.tm.repository.SessionRepository;

import java.sql.Connection;

public class SessionService extends AbstractUserOwnerService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull final IUserOwnerRepository<Session> getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

}
