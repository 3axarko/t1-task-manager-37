package ru.t1.zkovalenko.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.constant.TableConstant;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @Getter
    @NotNull
    private final String tableName = TableConstant.TABLE_TASK;

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    protected List<Task> fetch(@NotNull final ResultSet row) {
        @NotNull List<Task> tasks = new ArrayList<>();
        while (row.next()) {
            @NotNull final Task task = new Task();
            task.setId(row.getString(TableConstant.FIELD_ID));
            task.setCreated(row.getTimestamp(TableConstant.FIELD_CREATED));
            task.setUserId(row.getString(TableConstant.FIELD_USER_ID));
            task.setName(row.getString(TableConstant.FIELD_NAME));
            task.setDescription(row.getString(TableConstant.FIELD_DESCRIPTION));
            @Nullable final Status status = Status.toStatus(row.getString(TableConstant.FIELD_STATUS));
            task.setStatus(status == null ? Status.NOT_STARTED : status);
            task.setProjectId(row.getString(TableConstant.FIELD_PROJECT_ID));
            tasks.add(task);
        }
        return tasks;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?)",
                getTableName(),
                TableConstant.FIELD_ID,
                TableConstant.FIELD_CREATED,
                TableConstant.FIELD_USER_ID,
                TableConstant.FIELD_NAME,
                TableConstant.FIELD_DESCRIPTION,
                TableConstant.FIELD_STATUS,
                TableConstant.FIELD_PROJECT_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setTimestamp(2, new Timestamp(task.getCreated().getTime()));
            statement.setString(3, task.getUserId());
            statement.setString(4, task.getName());
            statement.setString(5, task.getDescription());
            statement.setString(6, Status.NOT_STARTED.name());
            statement.setString(7, task.getProjectId());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    public Task add(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task update(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ? where %s = ?",
                getTableName(),
                TableConstant.FIELD_USER_ID,
                TableConstant.FIELD_NAME,
                TableConstant.FIELD_DESCRIPTION,
                TableConstant.FIELD_STATUS,
                TableConstant.FIELD_PROJECT_ID,
                TableConstant.FIELD_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getUserId());
            statement.setString(2, task.getName());
            statement.setString(3, task.getDescription());
            statement.setString(4, task.getStatus().name());
            statement.setString(5, task.getProjectId());
            statement.setString(6, task.getId());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId,
                       @NotNull final String name,
                       @Nullable final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description == null ? "" : description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final ResultSet resultSet;
        @NotNull final String sql = String.format(
                "SELECT * FROM %s where %s = ? AND %s = ? order by %s",
                getTableName(),
                TableConstant.FIELD_USER_ID,
                TableConstant.FIELD_PROJECT_ID,
                getOrderByField()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            resultSet = statement.executeQuery();
            return fetch(resultSet);
        }
    }

    @Override
    @SneakyThrows
    public void removeAllByProjectId(@NotNull final String projectId) {
        @NotNull final String sql = String.format(
                "DELETE FROM %s where %s = ?",
                getTableName(),
                TableConstant.FIELD_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, projectId);
            statement.executeUpdate();
        }
    }

}
