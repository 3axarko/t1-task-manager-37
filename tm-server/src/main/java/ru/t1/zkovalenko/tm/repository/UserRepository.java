package ru.t1.zkovalenko.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.constant.TableConstant;
import ru.t1.zkovalenko.tm.api.repository.IUserRepository;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Getter
    @NotNull
    private final String tableName = TableConstant.TABLE_USER;

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    protected List<User> fetch(@NotNull final ResultSet row) {
        @NotNull List<User> users = new ArrayList<>();
        while (row.next()) {
            @NotNull final User user = new User();
            user.setId(row.getString(TableConstant.FIELD_ID));
            user.setLogin(row.getString(TableConstant.FIELD_LOGIN));
            user.setPasswordHash(row.getString(TableConstant.FIELD_PASSWORD));
            user.setEmail(row.getString(TableConstant.FIELD_EMAIL));
            user.setFirstName(row.getString(TableConstant.FIELD_FIRST_NAME));
            user.setLastName(row.getString(TableConstant.FIELD_LAST_NAME));
            user.setMiddleName(row.getString(TableConstant.FIELD_MIDDLE_NAME));
            user.setLocked(row.getBoolean(TableConstant.FIELD_LOCKED_FLG));
            @Nullable final Role role = Role.toRole(row.getString(TableConstant.FIELD_ROLE));
            user.setRole(role == null ? Role.USUAL : role);
            users.add(user);
        }
        return users;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, login, password, email, first_name, last_name, middle_name, role, locked_flg)"
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getMiddleName());
            statement.setString(8, user.getRole().name());
            statement.setBoolean(9, user.getLocked());
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? where %s = ?",
                getTableName(),
                TableConstant.FIELD_LOGIN,
                TableConstant.FIELD_PASSWORD,
                TableConstant.FIELD_EMAIL,
                TableConstant.FIELD_FIRST_NAME,
                TableConstant.FIELD_LAST_NAME,
                TableConstant.FIELD_MIDDLE_NAME,
                TableConstant.FIELD_ROLE,
                TableConstant.FIELD_LOCKED_FLG,
                TableConstant.FIELD_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getRole().name());
            statement.setBoolean(8, user.getLocked());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final IPropertyService propertyService,
            @NotNull final String login,
            @NotNull final String password
    ) {
        return create(propertyService, login, password, null, null);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final IPropertyService propertyService,
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String email
    ) {
        return create(propertyService, login, password, email, null);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final IPropertyService propertyService,
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) {
        return create(propertyService, login, password, null, role);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final IPropertyService propertyService,
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        @NotNull final User user = new User();
        user.setLogin(login);
        @Nullable final String saltPass = HashUtil.salt(propertyService, password);
        user.setPasswordHash(saltPass == null ? "" : saltPass);
        if (role != null) user.setRole(role);
        if (email != null) user.setEmail(email);
        return add(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final List<User> result;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                TableConstant.FIELD_LOGIN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            result = fetch(rowSet);
            if (result.isEmpty()) return null;
            return result.get(0);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final List<User> result;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                TableConstant.FIELD_EMAIL);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            result = fetch(rowSet);
        }
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
