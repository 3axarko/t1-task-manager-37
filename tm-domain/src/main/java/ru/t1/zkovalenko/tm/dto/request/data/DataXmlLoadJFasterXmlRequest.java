package ru.t1.zkovalenko.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;

@NoArgsConstructor
public final class DataXmlLoadJFasterXmlRequest extends AbstractUserRequest {

    public DataXmlLoadJFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}
