package ru.t1.zkovalenko.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.endpoint.IDomainEndpoint;
import ru.t1.zkovalenko.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected IDomainEndpoint getDomainEndpoint() {
        return serviceLocator.getDomainEndpoint();
    }

}
